import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class BigBuyFooter extends StatefulWidget {
  const BigBuyFooter({super.key});

  @override
  State<BigBuyFooter> createState() => _BigBuyFooterState();
}

class _BigBuyFooterState extends State<BigBuyFooter> {
  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;

    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Image.asset(
                    "images/bigbuy.png",
                    width: screenWidth / 3,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Social Media',
                  style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                      fontSize: screenWidth / 24,
                      color: Colors.black54,
                    ),
                  ),
                ),
                SizedBox(
                  height: 5.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Image.asset(
                      "images/fb.png",
                      width: screenWidth / 11,
                    ),
                    SizedBox(
                      width: 5.0,
                    ),
                    Image.asset(
                      "images/twitter.png",
                      width: screenWidth / 11,
                    ),
                    SizedBox(
                      width: 5.0,
                    ),
                    Image.asset(
                      "images/instagram.png",
                      width: screenWidth / 11,
                    ),
                    SizedBox(
                      width: 5.0,
                    ),
                    Image.asset(
                      "images/youtube.png",
                      width: screenWidth / 11,
                    ),
                    SizedBox(
                      width: 5.0,
                    ),
                    Image.asset(
                      "images/pinterest.png",
                      width: screenWidth / 11,
                    ),
                  ],
                ),
                SizedBox(
                  height: 5.0,
                ),
                Text(
                  'Got Question? Call us 24/7',
                  style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                      fontSize: screenWidth / 24,
                      color: Colors.black54,
                    ),
                  ),
                ),
                SizedBox(
                  height: 5.0,
                ),
                Text(
                  '01944996633',
                  style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                      fontSize: screenWidth / 16,
                      color: Colors.blue,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Payment Method',
                  style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                      fontSize: screenWidth / 24,
                      color: Colors.black54,
                    ),
                  ),
                ),
                SizedBox(
                  height: 5.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Image.asset(
                      "images/visa.png",
                      width: screenWidth / 11,
                    ),
                    SizedBox(
                      width: 5.0,
                    ),
                    Image.asset(
                      "images/mastercard.jpg",
                      width: screenWidth / 11,
                    ),
                    SizedBox(
                      width: 5.0,
                    ),
                    Image.asset(
                      "images/paypal.png",
                      width: screenWidth / 11,
                    ),
                    SizedBox(
                      width: 5.0,
                    ),
                    Image.asset(
                      "images/american_express.png",
                      width: screenWidth / 11,
                    ),
                    SizedBox(
                      width: 5.0,
                    ),
                    Image.asset(
                      "images/apple_pay.png",
                      width: screenWidth / 11,
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),

                //------------------ Information Section ----------------
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Information',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            fontSize: screenWidth / 22,
                            fontWeight: FontWeight.bold,
                            color: Colors.black87,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Text(
                        'About Big Buy',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            fontSize: screenWidth / 28,
                            color: Colors.black54,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 7,
                      ),
                      Text(
                        'FAQ',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            fontSize: screenWidth / 28,
                            color: Colors.black54,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 7,
                      ),
                      Text(
                        'Contact us',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            fontSize: screenWidth / 28,
                            color: Colors.black54,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 7,
                      ),
                      Text(
                        'Log in',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            fontSize: screenWidth / 28,
                            color: Colors.black54,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),

                //------------------ Customer Service ----------------
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Customer Service',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            fontSize: screenWidth / 22,
                            fontWeight: FontWeight.bold,
                            color: Colors.black87,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Text(
                        'Payment Methods',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            fontSize: screenWidth / 28,
                            color: Colors.black54,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 7,
                      ),
                      Text(
                        'Money-back gurantee!',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            fontSize: screenWidth / 28,
                            color: Colors.black54,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 7,
                      ),
                      Text(
                        'Returns',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            fontSize: screenWidth / 28,
                            color: Colors.black54,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 7,
                      ),
                      Text(
                        'Shipping',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            fontSize: screenWidth / 28,
                            color: Colors.black54,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 7,
                      ),
                      Text(
                        'Terms and conditions',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            fontSize: screenWidth / 28,
                            color: Colors.black54,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 7,
                      ),
                      Text(
                        'Privacy Policy',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            fontSize: screenWidth / 28,
                            color: Colors.black54,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                //------------------ My Account ----------------
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'My Account',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            fontSize: screenWidth / 22,
                            fontWeight: FontWeight.bold,
                            color: Colors.black87,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Text(
                        'Sign In',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            fontSize: screenWidth / 28,
                            color: Colors.black54,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 7,
                      ),
                      Text(
                        'View Cart',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            fontSize: screenWidth / 28,
                            color: Colors.black54,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 7,
                      ),
                      Text(
                        'My Wishlist',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            fontSize: screenWidth / 28,
                            color: Colors.black54,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 7,
                      ),
                      Text(
                        'Track My Order',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            fontSize: screenWidth / 28,
                            color: Colors.black54,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 7,
                      ),
                      Text(
                        'Help',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            fontSize: screenWidth / 28,
                            color: Colors.black54,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                //----------------Footer Section--------------
                Divider(
                  color: Colors.black54,
                ),
                SizedBox(
                  height: 5.0,
                ),
                Container(
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Design & Developed By ',
                            style: GoogleFonts.poppins(
                              textStyle: TextStyle(
                                fontSize: screenWidth / 28,
                                color: Colors.black54,
                              ),
                            ),
                          ),
                          Text(
                            'Link-Up Technology Ltd',
                            style: GoogleFonts.poppins(
                              textStyle: TextStyle(
                                decoration: TextDecoration.underline,
                                fontSize: screenWidth / 28,
                                color: Colors.blue,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Terms Of Use | ',
                            style: GoogleFonts.poppins(
                              textStyle: TextStyle(
                                fontSize: screenWidth / 28,
                                color: Colors.black54,
                              ),
                            ),
                          ),
                          Text(
                            'Privacy Policy',
                            style: GoogleFonts.poppins(
                              textStyle: TextStyle(
                                decoration: TextDecoration.underline,
                                fontSize: screenWidth / 28,
                                color: Colors.blue,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        'Copyright © 2023 Big Buy. All Rights Reserved.',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            fontSize: screenWidth / 28,
                            color: Colors.black54,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
